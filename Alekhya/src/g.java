class GFG  
	{ 
	  
	    // Calculate the sum  
	    // of proper divisors 
	    static String sumOfDiv(int x) 
	    { 
	  String str = "";
	        // 1 is a proper divisor 
	        int sum = 1; 
	        for (int i = 2; i <= Math.sqrt(x); i++)  
	        { 
	            if (x % i == 0)  
	            { 
	                sum += i; 
	  
	                // To handle perfect squares 
	                if (x / i != i) 
	                    sum += x / i; 
	            } 
	            str = str + i;
	        } 
	  
	        return str; 
	    } 
	  
	    // Check if pair is amicable 
	    static boolean isAmicable(int a, int b) 
	    { 
	        return (sumOfDiv(a) == b &&  
	                sumOfDiv(b) == a); 
	    } 
	  
	    // This function prints pair 
	    //  of amicable pairs present 
	    // in the input array 
	   
	  
	    // Driver code 
	    public static void main(String args[]) 
	    { 
	  
	  
	       
	    } 
	} 


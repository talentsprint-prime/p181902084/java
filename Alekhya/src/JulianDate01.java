public class JulianDate01 {
    public static void main(String[] args) {
        String date = "23-Jan-2016";
        System.out.println(dateFormat(date));
    }
   
    public static String dateFormat(String date) {
    	
    	String[]  dates = date.split("-");
        
        int d = Integer.parseInt(dates[0]);
        int m = convertMMMtoMM(dates[1]);
        int y = Integer.parseInt(dates[2]);
        return y + julianDate(d, m);
    }
   
   public static String julianDate(int dd, int mon) {
	   int [] months = {0 , 31 , 59 , 90 , 120 , 151 , 181, 212 ,243 , 273,304 ,334};
	  
	   int jDate = months[mon - 1] + dd ;
	   String day = "" + jDate ;
	   
	   if(jDate <= 9) {
		   day = "00" + jDate ;
	 } else if (jDate <= 99) {
		 day = "0" + jDate ;
	 }
	   return day;
      
   }
   
   public static int convertMMMtoMM(String mon) {
	   String months = "janfebmaraprmayjunjulaugsepoctnovdec";
   	
    	mon = mon.substring(0,3);
    	mon = mon.toLowerCase();
   	
   	return ((months.indexOf(mon)/3) + 1);
   }
}

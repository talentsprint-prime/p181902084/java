public class TwinPrimes01 {
	public static void main(String[] args) {

		int num1 = 1;
		int num2 = 200;
		// System.out.println(isPrime(num2));
		System.out.println(twinPrimes(num1, num2));
	}

	public static String twinPrimes(int start, int limit) {
		// ADD YOUR CODE HERE
		String str = "";

		if (start <= 0 || limit <= 0)
			return "-1";

		if (start >= limit)
			return "-2";
		
		
		for (int i = start; i < limit; i++) {
			
			
			if (isPrime(i) && isPrime(i + 2))
				str = str + i + ":" + (i + 2) + ",";

		}
		
		str = str.substring(0, str.length() - 1);
		
		if (str.length() == 0)
			return "-3";
		
		return str;

	}

	public static boolean isPrime(int num) {

		int count = 0;
		
		if(num == 1) {
			return false;
		}
		
		for (int i = 2; i <= num / 2; i++) {
			
			if (num % i == 0)
				count++;
			
			if (count > 0) 
				return false;
			
		}
		return true;

		// ADD YOUR CODE HERE
	}
}

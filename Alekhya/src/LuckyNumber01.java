public class LuckyNumber01 {
    public static void main(String[] args) {
        
    	String date = "15-March-2016";
        
    	System.out.println(getLuckyNumber(date));
    }

    public static int getLuckyNumber(String date) {
        
    	String[]  dates = date.split("-");
       
        int d = Integer.parseInt(dates[0]);
        int m = convertMMMtoMM(dates[1]);
        int y = Integer.parseInt(dates[2]);
        
        int sumOfDigits = getSumOfDigits(d);
        sumOfDigits += getSumOfDigits(m);
        sumOfDigits += getSumOfDigits(y);
        
        while(sumOfDigits > 10) {
        	
        	sumOfDigits = getSumOfDigits(sumOfDigits);
        
        }return sumOfDigits;
        
    }

    public static int convertMMMtoMM(String mon) {
         
    	String months = "janfebmaraprmayjunjulaugsepoctnovdec";
    	
    	mon = mon.substring(0,3);
    	mon = mon.toLowerCase();
    	
    	return ((months.indexOf(mon)/3) + 1);
    	
    	
    }
    
    public static int getSumOfDigits(int num) {
         
    	int sum = 0;
    	
    	while (num > 0) {
    		
    		sum +=num % 10;
    		num /= 10;
    	
    	}return sum;
    }
}

package assignment;

public class RangeWithStep {
	public static void main(String[] args) {
		int n1 = 1;
		int n2 = 200;
		int step = 23;
		System.out.println(getNumbersInRange(n1, n2, step));
	}

	public static String getNumbersInRange(int num1, int num2, int step) {

		String str = "";
		
		
			for (int i = num1 + 1; i < num2; i += step) {

				str = str + i + " ";
			}

		
		return str;
	}
}
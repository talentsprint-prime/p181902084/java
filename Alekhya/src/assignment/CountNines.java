package assignment;

public class CountNines {
	
		public static int getCountNines(int a, int b) {
			
			    int count = 0;
			    if(a < 0 || b < 0) {
			    	count = -1;
			    }
			    else if (a == 0 || b == 0){
			    	count = -2;
			    }
			    
			    else if (a < b){
			    for (int i = a; i <= b; i++) {
			        int j = i;
			        while (j > 0) {
			            if (j % 10 == 9){
			                count++;
			            j /= 10;
			            }
			           
			        }
			    }
			    }
			    else if (a > b){
				    for (int i = a; i >= b; i--) {
				        int j = i;
				        while (j > 0) {
				            if (j % 10 == 9){
				                count++;
				            j /= 10;
				        }
				           				    }
			    }
			    }
			    return count;
			}
		
		

		public static void main(String[] args) {
			System.out.println(getCountNines(30,21 ));

		}

	}


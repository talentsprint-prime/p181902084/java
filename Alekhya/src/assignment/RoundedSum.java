package assignment;

public class RoundedSum {
	public static void main(String[] args) {
		
		int a = 20, b = 30, c = 40;
		
		System.out.println(sumOfRoundedValues(a, b, c));
	}

	public static int sumOfRoundedValues(int n1, int n2, int n3) {
		
		int digit1 = 0;
		int digit2 = 0;
		int digit3 = 0;
		int sum1 = 0, sum2 = 0, sum3 = 0;
		
		if (n1 <= 0 || n2 <= 0 || n3 <= 0)
			return -1;
		
		digit1 = n1 % 10;
		if (digit1 < 5) {
			sum1 = ((n1 / 10) * 10);
			System.out.println(sum1);
		} else if (digit1 >= 5) {
			sum1 = ((n1 / 10 + 1) * 10);
			System.out.println(sum1);
		}
		
		digit2 = n2 % 10;
		if (digit2 < 5) {
			sum2 = ((n2 / 10) * 10);
			System.out.println(sum2);
		} else if (digit2 >= 5) {
			sum2 = ((n2 / 10 + 1) * 10);
			System.out.println(sum2);
		}
		
		digit3 = n3 % 10;
		if (digit3 < 5) {
			sum3 = ((n3 / 10) * 10);
			System.out.println(sum3);
		} else if (digit3 >= 5) {
			sum3 = ((n3 / 10 + 1) * 10);
			System.out.println(sum3);

		}
		
		return (sum1 + sum2 + sum3);

	}
}

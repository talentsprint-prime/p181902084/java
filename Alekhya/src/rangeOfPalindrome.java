import java.util.Scanner;

public class rangeOfPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
int begin ,limit;
Scanner sc = new Scanner(System.in);
System.out.println("enter range of numbers");
begin = sc.nextInt();
limit = sc.nextInt();
System.out.println(generateOddPalindrome(begin,limit));
	}

	public static int reverse(int num){
		
		int digit,rev =0;
		while(num>0){
		digit = num%10;
		rev = rev*10 + digit;
		digit = num/10;
		
	}
	return rev;
}
	public static boolean isPalindrome(int num){
	return	(reverse(num)==num);
	}
	public static boolean isAllDigitsOdd(int num){
		int d;
		while(num>0){
		d = num%10;
		if(d%2==0){
			return false;
			
		}
		num = num/10;
		}
		return true;
	}
	public static String generateOddPalindrome(int start,int end){
		String str = " ";
		int n;
		for(n=start;n<=end;n++)
		{
			if(isPalindrome(n)){
				if(isAllDigitsOdd(n)){
					
					str = str + " " + n;
				}
				
				
			}
			
		}
		return str;
	}
		
}

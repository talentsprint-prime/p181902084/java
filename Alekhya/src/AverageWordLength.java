
public class AverageWordLength {

	public static void main(String[] args) {
		System.out.println(AverageWordLength.getAverageWordLength("Hi mom"));
		System.out.println(AverageWordLength.getAverageWordLength("hi everyone"));
		System.out.println(AverageWordLength.getAverageWordLength("how are you"));

	}

	public static int getAverageWordLength(String str) {
		if (str == null) {
			return -1;
		}
		if (str == "") {
			return 0;
		}
		
		int count = 1;
		int countspace = 0;
		int len = str.length();
		int avgWordLength = 0;

		for (int i = 0; i <= str.length() - 1; i++) {

			if ((str.charAt(i) == ' ') && (str.charAt(i + 1) != ' ')) {
				count++;
			}
			if ((str.charAt(i) == ' ')) {
				countspace++;
			}
			avgWordLength = (len - countspace) / count;
		}
		return avgWordLength;

	}

}

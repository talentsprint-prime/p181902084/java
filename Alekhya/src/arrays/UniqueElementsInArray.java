package arrays;

import java.util.Scanner;

public class UniqueElementsInArray {

	public static void printDistinct(int arr[]) {
		for (int i = 0; i < arr.length; i++) {
			int j;
			for (j = 0; j < i; j++)
				if (arr[i] == arr[j])
					break;
			if (i == j)
				System.out.print(arr[i] + " ");
		}
	}

	public static void main(String[] args) {
		int[] arr = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter elements:");
		for(int i = 0;i < arr.length;i++)
		{
			arr[i] = sc.nextInt();
		}
		
		printDistinct(arr);

	}
}

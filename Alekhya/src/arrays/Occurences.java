package arrays;
import java.util.Scanner;

public class Occurences {

	
	public static void main(String[] args) {
			int[] arr = new int[5];
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter elements:");
			for(int i = 0;i < arr.length;i++)
			{
				arr[i] = sc.nextInt();
			}
			System.out.println("Enter element to search");
			int element = sc.nextInt();
			
			System.out.println(occurences(arr,element));
			}

	public static int occurences(int[] arr,int element){
			int count = 0;
			for(int i = 0;i < arr.length;i++){
				if(arr[i] == element){
					count++;
				}
			}
			return count;

	}
}
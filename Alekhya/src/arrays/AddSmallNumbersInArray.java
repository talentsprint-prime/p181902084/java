package arrays;

import java.util.Scanner;

public class AddSmallNumbersInArray {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);
		int[] arr = new int[5];
		System.out.println("Enter Array Elements");

		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println("Sum Of two smallest num in array is " + add2SmallNum(arr));
	}

	public static int add2SmallNum(int arr[]) {

		int firstMin = arr[0], secMin = arr[0];

		if (arr[0] < arr[1]) {
			firstMin = arr[0];
			secMin = arr[1];
		} else {
			firstMin = arr[1];
			secMin = arr[0];
		}

		for (int i = 2; i < arr.length; i++) {
			if (arr[i] < firstMin) {
				secMin = firstMin;
				firstMin = arr[i];
			} else if ((arr[i] < secMin) && arr[i] != firstMin) {
				secMin = arr[i];
			}
		}

		return firstMin + secMin;

	}

}

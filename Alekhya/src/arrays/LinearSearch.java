package arrays;

import java.util.Scanner;

public class LinearSearch {

	public static void main(String[] args) {
		
		int arr[] = { 1, 2, 7, 5, 6 };
		Scanner sc = new Scanner(System.in);
		System.out.println("enter value");
		int value = sc.nextInt();
		
		if (searchElement(arr, value))
			System.out.print("found the element");
		else
			System.out.println("not found the element");
	}

	public static boolean searchElement(int arr[], int value) {
		
		for (int i = 0; i < arr.length; i++) {
			if (value == arr[i])
				return true;
		}
		return false;
	}
}

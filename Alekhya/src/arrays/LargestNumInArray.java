package arrays;

import java.util.Scanner;

public class LargestNumInArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] arr = new int[5];
		System.out.println("Enter Array Elements");

		for (int j = 0; j < 5; j++) {
			arr[j] = sc.nextInt();
		}
		System.out.println("Largest number in array is " + maxNumber(arr));
	}

	public static int maxNumber(int arr[]) {
		
		int max = 0;

		for (int i = 0; i < arr.length; i++) {
			
			if (max < arr[i])
				max = arr[i];
		}
				return max;
	}

}

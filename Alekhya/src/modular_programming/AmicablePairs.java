package modular_programming;

public class AmicablePairs {

	public static void main(String[] args) {

		System.out.println(sixDigitAmicablePair(100000, 999999));

	}

	public static int factorsSum(int num) {

		int sum = 0;

		for (int i = 2; i * i <= num; i++) {
			if (i * i == num)
				sum = sum + i;
			else if (num % i == 0) {
				sum = sum + i + num / i;
			}
		}
		return sum + 1;
	}

	/*
	 * public static boolean isAmicable(int n1, int n2) {
	 * 
	 * int sum1 = 0; int sum2 = 0; int i, j;
	 * 
	 * sum1 = sum1 + factorsSum(n1); sum2 = sum2 + factorsSum(n2);
	 * 
	 * if (sum1 == n2 && sum2 == n1) { return true; } return false;
	 * 
	 * }
	 */

	public static String sixDigitAmicablePair(int start, int end) {

		String str = "";

		for (int i = start; i <= end; i++) {

			int num = factorsSum(i);

			if (num > i && num < end) {
				if (factorsSum(num) == i) {
					str = str + i + ":" + num + "\n";
				}
			}
		}
		return str;

	}
}

package modular_programming;

public class PerfectSquare4Digit {

	public static void main(String[] args) {

		System.out.println(rangeOfPerfectSquares(1000, 9999));

	}

	public static boolean isAllDigitsEven(int num) {

		int digit = 0;

		while (num > 0) {
			digit = num % 10;
			if (digit % 2 != 0) {
				return false;
			}
			num = num / 10;

		}

		return true;
	}

	/*
	 * public static boolean isperfectSquare(int num){
	 * 
	 * int square;
	 * 
	 * square = (int) Math.sqrt(num);
	 * 
	 * if(num == Math.pow(square, 2)) { return true; } return false;
	 * 
	 * }
	 */

	public static String rangeOfPerfectSquares(int start, int end) {

		String str = "";
		int num = 1;

		for (int i = (int) Math.sqrt(start) + 1; i <= (int) Math.sqrt(end); i += 2) {
			num = i * i;
			if (isAllDigitsEven(i)) {
				str = str + num + "\n";
			}
		}

		return str;
	}

}

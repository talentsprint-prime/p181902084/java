package modular_programming;

public class PrimeNumberInRange {

	public static void main(String[] args) {
		System.out.println(getPrimeInRange(1,20));
	}
	
	public static boolean isPrime(int num) {
		
		int count = 0;
		if(num == 1)
			return false;
		for(int i = 2 ; i <= num/2 ; i++) {
			if(num % i == 0) 
				count ++;
			if(count != 0)
				return false;
		}return true;
	}
	
	public static String getPrimeInRange(int start , int limit) {
		String str = "";
		for(int i = start ; i <= limit ; i++) {
			
			if(isPrime(i)) {
				str = str + i + " ";
			}
		}
		return str;
	}
}

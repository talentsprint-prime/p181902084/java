package modular_programming;

import java.util.Arrays;

public class LargestPower2Collatz {

	public static void main(String[] args) {
		
		
		System.out.println( highestPower2(9));
	}

	public static String getCollatz(int num1) {

		String str = "";
		int count = 0;

		while (num1 != 4) {
			count++;
			str = str + num1 + " ";

			if (num1 % 2 != 0) {
				num1 = 3 * num1 + 1;
			}else {
				num1 = num1 / 2;
			}

		}
		str = str + num1 + " 2 1";
		return str;
	}

	public static int highestPower2(int num) {
		
		String str = "";
		str = getCollatz(num);
		int count = 0;

		String[] s = str.split(" ");
		int len = s.length;
		int x = len - 2;
		
		while (Integer.parseInt(s[x]) % 2 == 0) {
			x--;
			}

		int num1 = Integer.parseInt(s[x + 1]);
		
		
		return num1;

	}
	 

}

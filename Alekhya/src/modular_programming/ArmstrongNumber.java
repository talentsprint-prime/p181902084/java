package modular_programming;

import java.util.Scanner;

public class ArmstrongNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      
	  int start , end; 
      
	  Scanner sc = new Scanner(System.in);
      start = sc.nextInt();
      end = sc.nextInt();
      
	System.out.println(getArmstrongInRange(start, end));
    
	}
	
	public static int getCountOfDigits(int num) {
		int count = 0;
		
		{
		while(num != 0){
			count ++;
			num = num / 10 ;
		  }
		}
		return count;
		
	}
	
    public static boolean isArmstrong(int num ) {
        
    	int result = 0;
    	int rem;
        int number = num;
        
        int x = getCountOfDigits(num);
        
        while (num != 0)
        {
            rem = num % 10;
            result += Math.pow(rem,x );
            num /= 10;
        }

       
        if(result == number) {
            return true;
        }
        else {
            return false;
        }
    }

    public static String getArmstrongInRange(int start,int end) {
    	  
    	String str =  "";
    	
    		for (int i = start + 1; i < end ; ++i) { 
    			
    			if(isArmstrong(i))
                str = str + i;
               	 }
    		return str;
    
    }
}

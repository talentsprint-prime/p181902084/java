package modular_programming;

public class StrongNumber {

	public static void main(String[] args) {
		if(isStrongNumber(146))
		System.out.println("Strong Number");
		else
			System.out.println("Not Strong Number");
	}
	
	public static int getFactorial(int num) {
		
		int fact = 1;
		
        for (int i=2; i<=num; i++) 
            fact *= i; 
        return fact; 
		}
		
	public static int getSum(int num) {
		int digit ;
		int sum = 0;
		while(num > 0) {
		digit = num % 10;
		sum = sum + getFactorial(digit);
		num = num / 10;
		}
		return sum;
		
	} 
	
	public static boolean isStrongNumber(int num) {
		if (num == getSum(num)) {
			return true;
		}
		return false;
	}
	}



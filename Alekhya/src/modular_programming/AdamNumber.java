package modular_programming;

public class AdamNumber {
	
	    public static void main(String[] args) {
	       
	        if(isAdamNumber(15))
	        System.out.println("Adam Number");
	        else
	        System.out.println("Not AdamNumber");
	    }
	    
	    
	    public static int getReverse(int n) {
	    	
	    int rev = 0;
	    	 
	    	while(n > 0) {
	             int digit = n % 10;
	             rev = rev * 10 + digit;
	             n = n/10 ;
	    	 }
			return rev; 
	    	
	    }

	   	    public static int getSquare(int n) {
			
	    	int square ;
	    	
	    	square = n*n ;
			
	    	return square;
	    	
	       	    }
	   	    
	   	 public static boolean isAdamNumber(int num) {
		    	
		    	int x = getReverse(num);
		    	int revsquare = getSquare(x);
		       
		    	revsquare = getReverse(revsquare);
		    	
		    	if(getSquare(num) == revsquare) 
				return true;
		    	else
				return false;	
				
		        
		    }
	}



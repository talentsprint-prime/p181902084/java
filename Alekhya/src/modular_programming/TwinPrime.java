package modular_programming;

import java.util.Scanner;

public class TwinPrime {

	
	public static void main(String[] args)  
    { 
    	int n1,n2;
    	Scanner sc = new Scanner(System.in);
    	n1 = sc.nextInt();
    	n2 = sc.nextInt();
    	
            if (twinPrime(n1, n2)) 
            System.out.println(rangeOfTwinPrimes(n1,n2)); 
        
    } 
	public static boolean isPrime(int n)  
    { 
        
          for(int i = 2;i <= n/2 ; i++) {
        	  if(n % i == 0)
        		  return true;
          }
        
                return false; 
 
    } 
  
    public static boolean twinPrime(int n1, int n2)  
    { 
        return (isPrime(n1) && isPrime(n2) && (n1 - n2) == 2);
                      
    } 
    
    
    public static String rangeOfTwinPrimes(int start,int end) {
    	String str = "";
    for(int i = start; i <= end; i++){
    	if (twinPrime(start, end)) {
    		str = str + i + i+2;
    		 
    	
    	}
    }return str;
    		
    	
    }
    
} 

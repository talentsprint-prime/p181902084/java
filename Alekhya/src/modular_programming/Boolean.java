package modular_programming;

import java.util.Scanner;

public class Boolean {

	public static void main(String[] args) {

		boolean a, b, c;
		Scanner sc = new Scanner(System.in);
		a = sc.nextBoolean();
		b = sc.nextBoolean();
		c = sc.nextBoolean();

		System.out.println(atLeastTwoTrue(a, b, c));

	}

	public static boolean atLeastTwoTrue(boolean a, boolean b, boolean c) {

		int count = 0;
		boolean b1 = false;

		if (a == true)
			count++;
		if (b == true)
			count++;
		if (c == true)
			count++;
		if (count == 2) {
			b1 = true;

		}

		return b1;
	}
}

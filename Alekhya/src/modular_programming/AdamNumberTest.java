package modular_programming;

import static org.junit.Assert.*;

import org.junit.Test;

public class AdamNumberTest {
	AdamNumber a = new AdamNumber();
	@Test
	public void testGetReverse() {
		assertEquals(123,a.getReverse(321));
		//fail("Not yet implemented");
	}

	@Test
	public void testGetSquare() {
		assertEquals(4,a.getSquare(2));
	}

	@Test
	public void testIsAdamNumber() {
		assertEquals(true, a.isAdamNumber(12));
	}
		

}

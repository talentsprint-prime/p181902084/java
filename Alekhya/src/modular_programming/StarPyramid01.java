package modular_programming;

import java.util.Scanner;

public class StarPyramid01 {
	
	public static void main(String[] args) {
			
		System.out.print(starPattern(5));
	
	}

	public static String starPattern(int rows) {
		
		String str = "";
		
		if(rows <= 0)
			return "-1";
        for (int i=1; i<=rows; i++) 
	         { 
		        for (int k=rows; k>i; k--) 
		            { 
		                str = str + " "; 
		            } 
		   
		            
		         for (int j=1; j<=i; j++ ) 
		            { 
		        	 	str = str + " *" ;
		            } 
		   
		          
		            str = str + "\n";
		       
	         } return str;
		   
	}
}

package student1;

public class Student {

	private int id;
	private String name;
	private int sub1;
	private int sub2;
	private int sub3;
	public static int idGenerator = 100;	
	public Student() {
		super();
		this.id = ++idGenerator;
	}
		
	public Student(String name, int sub1, int sub2, int sub3) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.sub1 = sub1;
		this.sub2 = sub2;
		this.sub3 = sub3;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSub1() {
		return sub1;
	}
	public void setSub1(int sub1) {
		this.sub1 = sub1;
	}
	public int getSub2() {
		return sub2;
	}
	public void setSub2(int sub2) {
		this.sub2 = sub2;
	}
	public int getSub3() {
		return sub3;
	}
	public void setSub3(int sub3) {
		this.sub3 = sub3;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", sub1=" + sub1 + ", sub2=" + sub2 + ", sub3=" + sub3 + ", calPercentage() = " +calPercentage() + "]";
	}
	
	public double calPercentage(){
		return ((sub1 + sub2 + sub3)/3);
	}
	public static  Student compare(Student s1 , Student s2 ,Student s3){
		int max = 0;
		Student max1 = null ;
		if(max < s1.calPercentage())
			max1 =  s1;
		if(max < s2.calPercentage())
			max1 = s2;
		if(max < s3.calPercentage())
			max1 =  s3;
		
		
		return max1;
	}

}

package interfaceprogram;

public class ElectricityBill implements Bill {
	
	public static final int per_unit_charge = 10;
	private int num_of_units;
	
	public ElectricityBill() {
		super();
	}
	
	public ElectricityBill(int num_of_units) {
		super();
		this.num_of_units = num_of_units;
	}
	
	

	public int getNum_of_units() {
		return num_of_units;
	}

	public void setNum_of_units(int num_of_units) {
		this.num_of_units = num_of_units;
	}

	public static int getPerUnitCharge() {
		return per_unit_charge;
	}

	@Override
	public double calculateBill() {
		
		return per_unit_charge*num_of_units;
	}

	@Override
	public void displayBill() {
		
			System.out.println("=========Electricity BIll=========");
			System.out.println("per_unit_charge = 10 " + "\n" +"number_of_units= " + num_of_units+ "\n" + "BillAmount = " + calculateBill() + "\n");

	}
	
	
	
}

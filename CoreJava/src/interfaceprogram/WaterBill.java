package interfaceprogram;

public class WaterBill implements Bill {
	
	public static final int per_unit_charge = 5;
	private int num_of_units;
	
	public WaterBill() {
		super();
	}
	public WaterBill(int num_of_units) {
		super();
		this.num_of_units = num_of_units;
	}
	
	
	public int getNum_of_units() {
		return num_of_units;
	}
	public void setNum_of_units(int num_of_units) {
		this.num_of_units = num_of_units;
	}
	public static int getPerUnitCharge() {
		return per_unit_charge;
	}
	@Override
	public double calculateBill() {
		
		return per_unit_charge*num_of_units;
	}
	
	@Override
	public void displayBill() {
		System.out.println("=========Water BIll==========");
		System.out.println("per_unit_charge = 5 " + "\n" +"number_of_units = " + num_of_units+ "\n" + "BillAmount = :" + calculateBill() + "\n");
	}
	
	}
	
	


package interfaceprogram;

import java.util.Scanner;

public class CalculateBillAmount {

	public static void main(String[] args) {
		int choice = 0;
		int units = 0;
		while (true) {
			System.out.println("1.WarerBill" + "\n" + "2.ElectricityBill" + "\n" + "3.Exit");
			Scanner sc = new Scanner(System.in);
			System.out.println("enter the choice : ");
			choice = sc.nextInt();
			if (choice != 3) {
				System.out.println("enter units : ");
				units = sc.nextInt();
			}
			switch (choice) {
			case 1:
				WaterBill b1 = new WaterBill();
				b1.setNum_of_units(units);
				b1.displayBill();
				break;
			case 2:
				ElectricityBill b2 = new ElectricityBill(10);
				b2.setNum_of_units(units);
				b2.displayBill();
				break;
			case 3:
				System.exit(0);

			default:
				System.out.println("Error");
			}
		}

	}
}

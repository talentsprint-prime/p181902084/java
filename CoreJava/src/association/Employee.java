package association;

public class Employee {
	private static int id;
	private String name;
	private double salary;
	private String designation;
	private static int idGenerator = 100;
	Address add;
	
	public Employee() {
		super();
		this.id = ++idGenerator;
	}


	public Employee(String name, double salary, String designation, Address add) {
		super();
		this.name = name;
		this.salary = salary;
		this.designation = designation;
		this.add = add;
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getSalary() {
		return salary;
	}


	public void setSalary(double salary) {
		this.salary = salary;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public static int getId() {
		return id;
	}


	public static int getIdGenerator() {
		return idGenerator;
	}


	@Override
	public String toString() {
		return "Employee [name=" + name + ", salary=" + salary + ", designation=" + designation + ",add=" + add+ "]";
	}


	public void setadd(Address add) {
		this.add = add;
	}

	
}

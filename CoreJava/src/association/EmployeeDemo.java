package association;

public class EmployeeDemo {

	public static void main(String[] args) {
		Address a1 = new Address("201" , "Nizampet" , "Hyderabad" , "Telangana" , 500072);
		Employee e1 = new Employee("alekhya", 60000, "developer", a1);
		
		System.out.println(e1);
		Address newAddr = new Address("302" , "Nizampet" , "Hyderabad" , "Telangana" , 500072) ;
		e1.setadd(newAddr);
		System.out.println(e1);
			}

}

package inheritance;

public class BatsMan extends Player {
	private int runsScored;
	private int ballsFaced;
	private int dismissals;

	public BatsMan() {
		super();
	}

	public BatsMan(String name, int matches, int runsScored, int ballsFaced, int dismissals) {
		super(name, matches);
		this.runsScored = runsScored;
		this.ballsFaced = ballsFaced;
		this.dismissals = dismissals;
	}

	public int getRunsScored() {
		return runsScored;
	}

	public void setRunsScored(int runsScored) {
		this.runsScored = runsScored;
	}

	public int getBallsFaced() {
		return ballsFaced;
	}

	public void setBallsFaced(int ballsFaced) {
		this.ballsFaced = ballsFaced;
	}

	public int getDismissals() {
		return dismissals;
	}

	public void setDismissals(int dismissals) {
		this.dismissals = dismissals;
	}

	public double cahStrikeRate() {
		return runsScored / ballsFaced;
	}

	public double calAverage() {
		return (runsScored / (double) ballsFaced) * 100;
	}

}
package inheritance;

public class Programmer extends Employee {

	private String progammingLanguage;
	
	public Programmer() {
		super();
	
		
	}

	public Programmer(String name, double salary, String address, long phoneNumber, String progammingLanguage) {
		super(name, salary, address);
		this.progammingLanguage = progammingLanguage;
	}

	public String getProgammingLanguage() {
		return progammingLanguage;
	}

	public void setProgammingLanguage(String progammingLanguage) {
		this.progammingLanguage = progammingLanguage;
	}

	@Override
	public String toString() {
		return "Programmer [progammingLanguage=" + progammingLanguage + ", toString()=" + super.toString() + "]";
	}
	
	public void work() {
		System.out.println("I am Programmer");
	}
	
}

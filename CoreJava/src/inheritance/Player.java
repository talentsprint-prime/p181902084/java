package inheritance;

public abstract class Player {
	 
	private int id;
	private String name;
	private int matches;
	
	private static int idGenerator = 100;
	
	public Player() {
		
		super();
		this.id = ++idGenerator;
	}


	public Player(String name, int matches) {
		super();
		this.name = name;
		this.matches = matches;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getMatches() {
		return matches;
	}


	public void setMatches(int matches) {
		this.matches = matches;
	}


	public static int getIdGenerator() {
		return idGenerator;
	}


	public int getId() {
		return id;
	}


	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", matches=" + matches + "]";
	}


	public abstract double calAverage() {
		
		
	}
	
	
	}


package inheritance;

public class EmployeeDemo {

	public static void main(String[] args) {
		
		Programmer p1 = new Programmer("ajay",25000,"gachibowli", 987783844 ,"java");
		System.out.println(p1);
		p1.work();
		
		System.out.println("programmer " +p1.getId());
		
		Employee emp1 = new Employee("vishal" , 25000 , "chandanagar" );
		emp1.work();
		Employee emp2 = new Employee("viha" , 15000 , "jj" );
		emp1.work();

	}

}

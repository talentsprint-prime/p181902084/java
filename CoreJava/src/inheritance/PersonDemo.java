package inheritance;

public class PersonDemo {
	public static void main(String[] args) {
		Person p1 = new Student("Alekhya", "hyderabad", "EIE", 2018, 100000);
		System.out.println(p1);
		Person p2 = new Staff("Shivani", "Chandanagar", "MGIT", 60000);
		System.out.println(p2);
	}

}

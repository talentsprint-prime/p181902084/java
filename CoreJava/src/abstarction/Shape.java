package abstarction;

public abstract class Shape {
	private String colour;
	private boolean filled;

	public Shape() {
		super();

	}

	public Shape(String colour, boolean filled) {
		super();
		this.colour = colour;
		this.filled = filled;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	abstract public double getArea();

	abstract public double getPerimeter();

	@Override
	public String toString() {
		return "Shapes [colour=" + colour + ", filled=" + filled + ", getArea()=" + getArea() + ", getPerimeter()="
				+ getPerimeter() + "]";
	}

}

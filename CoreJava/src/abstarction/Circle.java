package abstarction;

public class Circle extends Shape {

	private double radius;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Circle() {
		super();

	}

	public Circle(double radius) {
		super();
		this.radius = radius;

	}

	public Circle(double radius, String colour, boolean filled) {
		super(colour, filled);
		this.radius = radius;

	}

	public double getArea() {
		double area = Math.PI * radius * radius;
		return area;
	}

	public double getPerimeter() {
		double perimeter = 2 * Math.PI * radius;
		return perimeter;
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}

}

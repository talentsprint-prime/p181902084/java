package abstarction;

public class Customer {
	 private int id;
    private String name;
    private int accountNo;
    private double principle;
    private int tenure;
	
    public Customer() {
		super();
	}

	public Customer(int id, String name, int accountNo, double principle, int tenure) {
		super();
		this.id = id;
		this.name = name;
		this.accountNo = accountNo;
		this.principle = principle;
		this.tenure = tenure;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", accountNo=" + accountNo + ", principle=" + principle
				+ ", tenure=" + tenure + "]";
	}
    
    
    
    
    
    

}
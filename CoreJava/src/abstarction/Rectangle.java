package abstarction;

public class Rectangle extends Shape {

		public class k {

	}
		private double width;
		private double length;
		public Rectangle() {
		super();

		}
		public Rectangle(double width,double length){
		super();
		this.width = width;
		this.length = length;

		}

		public Rectangle(double width,double length,String colour, boolean filled) {
		super(colour, filled);
		this.width = width;
		this.length = length;

		}
		public double getWidth() {
		return width;
		}
		public void setWidth(double width) {
		this.width = width;
		}
		public double getLength() {
		return length;
		}
		public void setLength(double length) {
		this.length = length;
		}
		public double getArea() {
		double area = length * width;
		return area;
		}

		public double getPerimeter() {
		double perimeter = 2*(length+width);
		return perimeter;
		}
		@Override
		public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
		}



		}


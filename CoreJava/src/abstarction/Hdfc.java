package abstarction;

public class Hdfc implements Bank{
	
	private int id;
	private String branch;
	private Customer cust;
	public static final double rate_of_interest = 6;
	public Hdfc() {
		super();
	}
	
	public Hdfc(int id, String branch, Customer cust) {
		super();
		this.id = id;
		this.branch = branch;
		this.cust = cust;
	}

	public void setCust(Customer cust) {
		this.cust = cust;
	}
	public Customer getCust() {
		return cust;
	}
	

	
	@Override
	public double calSI() {
		
	
		return ((cust.getPrinciple()*cust.getTenure()*rate_of_interest)/100);
	}
	

}

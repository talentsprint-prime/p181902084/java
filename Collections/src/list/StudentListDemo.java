package list;

import java.util.ArrayList;

import java.util.List;

public class StudentListDemo {

	public static void main(String[] args) {

		List<StudentList> studentList = new ArrayList<>();

		StudentList s1 = new StudentList("alekhya", 60, 60, 60);
		studentList.add(s1);
		studentList.add(new StudentList("abc", 75, 85, 65));
		studentList.add(new StudentList("pqr", 55, 65, 60));

		for (StudentList s : studentList) {
			System.out.println("Id:" + s.getId() + " name:" + s.getName() + " percentage:" + s.percentage());

		}
	}

}


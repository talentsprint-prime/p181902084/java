package list;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {

		List<Integer> yearList = new ArrayList<>();

		yearList.add(2016);
		yearList.add(2017);
		yearList.add(2018);
		yearList.add(2019);
		yearList.add(2020);
		yearList.add(2021);

		for (Integer i : yearList) {
			System.out.println(i);
		}
//		System.out.println("removed" + yearList.remove(0));
//		for (Integer i : yearList) {
//			System.out.println(i);

		System.out.println(yearList.size());
		
		System.out.println(yearList.get(2));
		}
	}


package comparable_interface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class EmpSort {

	public static void main(String[] args) {
		
		List <EmployeeS> employeelist = new ArrayList<>();
		List <EmployeeS> fresherlist = new ArrayList<>();
		
		employeelist.add(new EmployeeS("Alekhya" , 55000));
		employeelist.add(new EmployeeS("Shivani" , 50000));
		employeelist.add(new EmployeeS("Sri" , 10000));
		Collections.sort(employeelist);
		
		Iterator<EmployeeS> itr = employeelist.iterator();
		while(itr.hasNext()) {
			EmployeeS e = itr.next();
			if(e.getSalary() < 20000) {
				itr.remove();
				fresherlist.add(e);
			}
		}
		
		for(EmployeeS e1 : employeelist) {
			System.out.println(e1);	
		}
		System.out.println("======");
		for(EmployeeS e2 : fresherlist) {
			System.out.println(e2);
		}
		
//		for (Employee e : employeelist) {
//			if (e.getSalary() < 20000)
//				employeelist.remove(e);
//		for(Employee e1 : employeelist)	{
//			 System.out.println(e1);
//		}
		}

}


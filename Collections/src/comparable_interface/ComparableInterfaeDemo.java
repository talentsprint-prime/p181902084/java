package comparable_interface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparableInterfaeDemo {

	public static void main(String[] args) {

		List<String> countryList = new ArrayList<>();

		countryList.add("India");
		countryList.add("USA");
		countryList.add("UK");
		countryList.add("England");
		countryList.add("Australia");
		System.out.println("===========BeforeSort========");
		for (String s : countryList) {
			System.out.println(s);
		}

		Collections.sort(countryList);

		System.out.println("===========Aftersort==========");
		for (String s : countryList) {
			System.out.println(s);
		}

	}

}

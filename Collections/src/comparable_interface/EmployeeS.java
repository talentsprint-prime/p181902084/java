package comparable_interface;

public class EmployeeS implements Comparable<EmployeeS>{
	private int id;
	private String name;
	private double Salary;
	
	public static int idGenerator = 100;

	public EmployeeS(String name, int i) {
		super();
		this.id = ++idGenerator;
	}

	public EmployeeS(String name, double salary) {
		super();
		this.name = name;
		Salary = salary;
		this.id = ++idGenerator;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return Salary;
	}

	public void setSalary(double salary) {
		Salary = salary;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", Salary=" + Salary + "]";
	}

	@Override
	
	public int compareTo(EmployeeS o) {
	if(this.getSalary() > o.getSalary())
	return 1;
	else if(this.getSalary() < o.getSalary())
	return -1;
	else
	return 0;
	}	
	
}

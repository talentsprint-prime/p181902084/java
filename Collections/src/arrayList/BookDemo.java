package arrayList;

import java.util.ArrayList;
import java.util.List;

public class BookDemo {
	public static void main(String[] args) {

		List<Book> bookList = new ArrayList<>();
		bookList.add(new Book("hannah montana", "alekhya", "gk"));
		bookList.add(new Book("how to win friends", "shiva", "vn"));
		bookList.add(new Book("men are from mars and women are from venus", "kalpana", "lk"));

		for (Book b : bookList) {
			if (b.getPublisher().equals("gk"))
				System.out.println(b);
		}

	}
}
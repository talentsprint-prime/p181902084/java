package arrayList;

public class Book {
	private int id;
	private String name;
	private String Author;
	private String Publisher;

	public static int idGenerator = 100;

	public Book() {
		super();
		this.id = ++idGenerator;
	}

	public Book(String name, String author, String publisher) {
		super();
		this.id = ++idGenerator;
		this.id = id;
		this.name = name;
		this.Author = author;
		this.Publisher = publisher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return Author;
	}

	public void setAuthor(String author) {
		this.Author = author;
	}

	public String getPublisher() {
		return Publisher;
	}

	public void setPublisher(String publisher) {
		this.Publisher = publisher;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", Author=" + Author + ", Publisher=" + Publisher + "]";
	}

}
package arrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class EmployeeDemo {

	public static void main(String[] args) {
		
		List <Employee> employeelist = new ArrayList<>();
		List <Employee> fresherlist = new ArrayList<>();
		
		employeelist.add(new Employee("Alekhya" , 55000));
		employeelist.add(new Employee("Shivani" , 50000));
		employeelist.add(new Employee("Sri" , 10000));
		Iterator<Employee> itr = employeelist.iterator();
		while(itr.hasNext()) {
			Employee e = itr.next();
			if(e.getSalary() < 20000) {
				itr.remove();
				fresherlist.add(e);
			}
		}
		
		for(Employee e1 : employeelist) {
			System.out.println(e1);	
		}
		System.out.println("======");
		for(Employee e2 : fresherlist) {
			System.out.println(e2);
		}
		
//		for (Employee e : employeelist) {
//			if (e.getSalary() < 20000)
//				employeelist.remove(e);
//		for(Employee e1 : employeelist)	{
//			 System.out.println(e1);
//		}
		}

}


package comparator_interface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MovieDemo {

	public static void main(String[] args) {

		List<Movies> movieList = new ArrayList<>();

		movieList.add(new Movies("GullyBoy", "ZoyaAkhtar", 140, 2019));
		movieList.add(new Movies("Sanju", "RajuHirani", 160, 2018));
		movieList.add(new Movies("Padmavat", "SanjayLeelaBhansali", 170, 2018));
		movieList.add(new Movies("Raazi", "MeghanaGulzhar", 150, 2018));
		movieList.add(new Movies("Zindagi Na Milegi Dubara", "ZoyaAkhtar", 150, 2011));
		movieList.add(new Movies("Two States", "AbhishekVarman", 120, 2014));
		movieList.add(new Movies("Student Of The Year", "Karan Johar", 135, 2012));
		movieList.add(new Movies("Dangal", "NiteshTiwari", 145, 2016));
		movieList.add(new Movies("Dhadak", "Shashank", 125, 2018));
		movieList.add(new Movies("Kalank", "Abhishek", 165, 2019));
		int choice = 0;

		while (true) {

			System.out.println("Enter Choice");
			System.out.println("1.Sort By Name \n" + "2.Sort By Diretor \n" + "3.Sort by duration\n" + "4.Sort by Year\n" + "5.Exit");

			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
			switch (choice) {

			case 1:
				Collections.sort(movieList, new SortByName());
				for (Movies m : movieList) {
					System.out.println(m);
				}
				break;
			case 2:
				Collections.sort(movieList, new SortByDirector());
				for (Movies m : movieList) {
					System.out.println(m);
				}
				break;

			case 3:
				Collections.sort(movieList, new SortByDuration());
				for (Movies m : movieList) {
					System.out.println(m);
				}
				break;

			case 4:
				Collections.sort(movieList, new SortByYear());
				for (Movies m : movieList) {
					System.out.println(m);
				}
				break;
			case 5:
				System.exit(0);
				break;
			default : System.out.println("invalid entry");	
			break;	
			}
		}
	}
}

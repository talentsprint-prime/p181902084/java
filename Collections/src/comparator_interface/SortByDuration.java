package comparator_interface;

import java.util.Comparator;


	public class SortByDuration implements Comparator<Movies> {

		@Override
		public int compare(Movies o1, Movies o2) {

			if (o1.getDuration() > o2.getDuration())
				return 1;
			else if (o1.getDuration() < o2.getDuration())
				return -1;
			else
				return 0;
		}

}

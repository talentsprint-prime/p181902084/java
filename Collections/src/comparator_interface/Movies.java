package comparator_interface;

public class Movies {
	
	private String name;
	private String director_Name;
	private int duration;
	private int releasedYear;

	
	public Movies(String name, String director_Name, int duration, int releasedYear) {
		super();
		this.name = name;
		this.director_Name = director_Name;
		this.duration = duration;
		this.releasedYear = releasedYear;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDirector_Name() {
		return director_Name;
	}


	public void setDirector_Name(String director_Name) {
		this.director_Name = director_Name;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public int getReleasedYear() {
		return releasedYear;
	}


	public void setReleasedYear(int releasedYear) {
		this.releasedYear = releasedYear;
	}


	@Override
	public String toString() {
		return "Movies [name=" + name + ", director_Name=" + director_Name + ", duration=" + duration
				+ ", releasedYear=" + releasedYear + "]";
	}
	
	
	
	
	
}

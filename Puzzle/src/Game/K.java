package Game;


public class K {

	private static char[][] spaces;
	static int row;
	static int col;

	K() {
		spaces = new char[5][];
		for (int i = 0; i < 5; i++) {
			spaces[i] = new char[5];
		}
	}

	public static void swapSpace(int row2, int col2) {
		char tmp = spaces[row][col];
		spaces[row][col] = spaces[row2][col2];
		spaces[row2][col2] = tmp;
	}

	
	public static boolean doMove(char c) {
		switch (c) {
		case 'A':
			if (row == 0)
				return false;
			swapSpace(row - 1, col);
			row--;
			return true;
		case 'B':
			if (row == 4)
				return false;
			swapSpace(row + 1, col);
			row++;
			return true;
		case 'L':
			if (col == 0)
				return false;
			swapSpace(row, col - 1);
			col--;
			return true;
		case 'R':
			if (col == 4)
				return false;
			swapSpace(row, col + 1);
			col++;
			return true;
		default:
			System.out.println("Bad move: '" + c + "'");
			return false;
		}
	}

}

package Game;

import static org.junit.Assert.*;

import org.junit.Test;

public class PuzzleTest {
	Puzzle p = new Puzzle();
	String str1 = "ABCDE" + "\n" + "FGHIJ" + "\n" + "KLMNO" + "\n" + "PQRST" + "\n" + "UVWXY" + "\n";
	String str2 = "ZXCVB" + "\n" + "NMGLK" + "\n" + "JH_FD" + "\n" + "SAQWE" + "\n" + "RTYUI" + "\n";
	@Test
	public void testGridPuzzle() {
	assertEquals(str1 , p.gridPuzzle("ABCDEFGHIJKLMNOPQRSTUVWXY"));	
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSwapSpace() {
		assertEquals("ABCDEFGHI_JKLMNOPQRSTUVWX" ,p.swapSpace('L', "ABCDEFGHIJ_KLMNOPQRSTUVWX") );
		//fail("Not yet implemented");
	}

	 @Test
	public void testDoMove() {
		 assertEquals(str2 ,p.doMove("ABBLR", "ZXCVBNM_LKJHGFDSAQWERTYUI"));
		//fail("Not yet implemented");
	} 

	@Test
	public void testIsValidMove() {
		assertEquals(true , p.isValidMove('A', "ABCDEF_GHIJKLMNOPQRSTUVWX"));
		//fail("Not yet implemented");
	} 

}
